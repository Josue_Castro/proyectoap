<?php
$filtro = $_GET['filtro'];
$log = new Log();
$logsA = $log -> consultarFiltroAdmin($filtro);
$logsC = $log -> consultarFiltroCli($filtro);
$logsP = $log -> consultarFiltroProv($filtro);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-danger">
					<h4>Consultar Log</h4>
				</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Actor</th>
							<th>Hora</th>
							<th>Fecha</th>
							<th>datos</th>
							<th>accion</th>
							<th></th>
						</tr>
						<?php 
						$i=1;
						foreach($logsA as $logActual){
						    $pos = stripos($logActual -> getActor(), $filtro);
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    if($pos === false){
						        echo "<td>" . $logActual -> getActor() . "</td>";						        
						    }else{						        
						        echo "<td>" . substr($logActual -> getActor(), 0, $pos) . "<mark>" . substr($logActual -> getActor(), $pos, strlen($filtro)) . "</mark>" . substr($logActual -> getActor(), $pos+strlen($filtro)) . "</td>";
						    }
						    echo "<td>" . $logActual -> getHora() . "</td>";
						    echo "<td>" . $logActual -> getFecha() . "</td>";
						    echo "<td>" . $logActual -> getDatos() . "</td>";
						    echo "<td>" . $logActual -> getAccion() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						
						foreach($logsC as $logActual){
						    $pos = stripos($logActual -> getActor(), $filtro);
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    if($pos === false){
						        echo "<td>" . $logActual -> getActor() . "</td>";
						    }else{
						        echo "<td>" . substr($logActual -> getActor(), 0, $pos) . "<mark>" . substr($logActual -> getActor(), $pos, strlen($filtro)) . "</mark>" . substr($logActual -> getActor(), $pos+strlen($filtro)) . "</td>";
						    }
						    echo "<td>" . $logActual -> getHora() . "</td>";
						    echo "<td>" . $logActual -> getFecha() . "</td>";
						    echo "<td>" . $logActual -> getDatos() . "</td>";
						    echo "<td>" . $logActual -> getAccion() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						
						foreach($logsP as $logActual){
						    $pos = stripos($logActual -> getActor(), $filtro);
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    if($pos === false){
						        echo "<td>" . $logActual -> getActor() . "</td>";
						    }else{
						        echo "<td>" . substr($logActual -> getActor(), 0, $pos) . "<mark>" . substr($logActual -> getActor(), $pos, strlen($filtro)) . "</mark>" . substr($logActual -> getActor(), $pos+strlen($filtro)) . "</td>";
						    }
						    echo "<td>" . $logActual -> getHora() . "</td>";
						    echo "<td>" . $logActual -> getFecha() . "</td>";
						    echo "<td>" . $logActual -> getDatos() . "</td>";
						    echo "<td>" . $logActual -> getAccion() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>
<?php
$proveedor= new Proveedor();
$proveedores = $proveedor -> consultarTodos();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-danger">
					<h4>Consultar Proveedores</h4>
				</div>
				<div class="text-right"><?php echo count($proveedores) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-md">
						<tr>
							<th>#</th>
							<th>Contacto</th>
							<th>Direccion</th>
							<th>Correo</th>
							<th>Estado</th>
							<th>Servicios</th>
						</tr>
						<?php 
						$i=1;
						foreach($proveedores as $proveedorActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $proveedorActual -> getContacto() . "</td>";
						    echo "<td>" . $proveedorActual -> getDireccion() . "</td>";
						    echo "<td>" . $proveedorActual -> getCorreo() . "</td>";
						    echo "<td>" . (($proveedorActual -> getEstado()==1)?"<div id='icono" . $proveedorActual -> getIdProveedor() . "'><span class='fas fa-check-circle' data-toggle='tooltip' data-placement='left' title='Habilitado'></span></div>":(($proveedorActual -> getEstado()==0)?"<div id='icono" . $proveedorActual -> getIdProveedor() . "'><span class='fas fa-times-circle' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></span></div>":"<span class='fas fa-ban' data-toggle='tooltip' data-placement='left' title='Inactivo'></span>")) . "</td>";						    
						    echo "<td><div id='accion" . $proveedorActual -> getIdProveedor() . "'><a id='cambiarEstado" . $proveedorActual -> getIdProveedor() . "' href='#' >" . (($proveedorActual -> getEstado()==1)?"<span class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitar'></span>":(($proveedorActual -> getEstado()==0)?"<span class='fas fa-user-check' data-toggle='tooltip' data-placement='left' title='Habilitar'></span>":"")) . "</a>";
						?>  
                        <script>
                        $(document).ready(function(){
                        	$("#cambiarEstado<?php echo $proveedorActual -> getIdProveedor() ?>").click(function(e){
                        		$('[data-toggle="tooltip"]').tooltip('hide');
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/proveedor/cambiarEstadoProveedorAjax.php") ?>&idProveedor=<?php echo $proveedorActual -> getIdProveedor() ?>&nuevoEstado=<?php echo (($proveedorActual -> getEstado()==1)?"0":"1")?>";		
                        		$("#icono<?php echo $proveedorActual -> getIdProveedor() ?>").load(url);
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/proveedor/cambiarEstadoAccionPAjax.php") ?>&idProveedor=<?php echo $proveedorActual -> getIdProveedor() ?>&nuevoEstado=<?php echo (($proveedorActual -> getEstado()==1)?"0":"1")?>";
                        		$("#accion<?php echo $proveedorActual -> getIdProveedor() ?>").load(url);
                        	});
                        });
                        </script>
						<?php   						    
						    echo "</div></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>
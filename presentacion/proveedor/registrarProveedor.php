<?php
if(isset($_POST["registrar"])){
    $Contacto = $_POST["Contacto"];
    $Direccion= $_POST["Direccion"];
    $correo = $_POST["correo"];
    $clave = $_POST["clave"];
    $proveedor = new Proveedor("", $Contacto, $Direccion, $correo, $clave, "");
    $proveedor -> registrar();
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-danger">
					<h4>Registro Proveedor</h4>
				</div>
              	<div class="card-body">
        			<form action="index.php?pid=<?php echo base64_encode("presentacion/proveedor/registrarProveedor.php") ?>" method="post">
        				<div class="form-group">
    						<input name="Contacto" type="text" class="form-control" placeholder="Contacto" required>
    					</div>
    					<div class="form-group">
    						<input name="Direccion" type="text" class="form-control" placeholder="Direccion" required>
    					</div>
        				<div class="form-group">
    						<input name="correo" type="email" class="form-control" placeholder="Correo" required>
    					</div>
        				<div class="form-group">
    						<input name="clave" type="password" class="form-control" placeholder="Clave" required>
    					</div>
        				<div class="form-group">
    						<input name="registrar" type="submit" class="form-control btn btn-danger">
    					</div>	    					
        			</form>     
            	</div>
            </div>		
		</div>
	</div>
</div>

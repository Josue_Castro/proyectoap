<?php 

if (isset($_GET["idProducto"])){
    
    $producto = new Producto($_GET["idProducto"]);
    $producto -> consultar();
}

?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-danger">
					<h4>Detalles del Producto</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["agregar"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					
					<form action="index.php?pid=<?php echo base64_encode("presentacion/carrito.php"). "&idProducto=" . $producto -> getIdProducto() ?>" method="post">
						
						<div class="form-group text-center">
							<?php echo (($producto -> getImagen()!="")?"<img src='" . $producto -> getImagen() . "' height='200px'>":"")?> 
							
						</div>
						<div class="form-group text-center">
							<h3><?php echo $producto -> getNombre()?></h3>
						</div>
						<div class="form-group text-center">
						<p><strong class="h4">$<?php echo $producto -> getPrecio()?></strong></p>
						</div>
						
						<div class="text-center">
							<button type="submit" name="agregar" class="btn btn-danger">Agregar</button>
						</div>
						<br>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>

<?php


    $producto = new Producto();
    $productos = $producto -> consultarTodos();
    $hora = date("h:i:s");
    $fecha = date("Y-m-d");
    
if(isset($_POST["prov"])){
    
    $inventario = "";
    if(isset($_POST["inventario"])){
        $inventario = $_POST["inventario"];
    }
    
    $prod = new Producto($_POST["Comprod"]);
    $prod -> consultar();

    $inv=$prod -> getCantidad() + $inventario;
    
    $producto = new Producto($_POST["Comprod"],"",$inv,"","");   
    $producto -> provCant();
    
    $proveedor = new Proveedor($_SESSION["id"]);
    $proveedor -> consultar();
    $Log = new Log("","Proveedor", $hora, $fecha, $proveedor -> getCorreo(),"Proveer Producto: " . $prod -> getNombre(), "");
    $Log->insertarProveedor($proveedor -> getIdProveedor());
    
    $proveedor -> ingresarProd_Prov($proveedor -> getIdProveedor(), $_POST["Comprod"]);    
}


?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-danger">
					<h4>Proveer Producto</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["prov"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos Ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/producto/proveerProducto.php") ?>" method="post">
						<div class="form-group">
							<label>Productos</label>
							<br> 
							<label>
								<select name="Comprod">
								<?php foreach ($productos as $selectProd){ ?>
								
										<option value="<?php echo $selectProd -> getIdProducto() ?>"><?php echo $selectProd -> getNombre() ?></option>
								
								<?php }?>
								</select>	
							</label>
						</div>
						<div class="form-group">
							<label>Cantidad</label> 
							<input type="number" name="inventario" class="form-control" min="1" value="<?php echo $cantidad ?>" required>
						</div>
						<button type="submit" name="prov" class="btn btn-danger">Proveer</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>
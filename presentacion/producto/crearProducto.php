<?php

$nombre = "";
if(isset($_POST["nombre"])){
    $nombre = $_POST["nombre"];
}

$precio = "";
if(isset($_POST["precio"])){
    $precio = $_POST["precio"];
}

$hora = date("h:i:s");
$fecha = date("Y-m-d");

if(isset($_POST["crear"])){
    
    $_FILES["imagen"]["name"];
    $rutaLocal = $_FILES["imagen"]["tmp_name"];
    $tipo = $_FILES["imagen"]["type"];
    $tiempo = new DateTime();
    $rutaRemota = "imagenes/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
    copy($rutaLocal,$rutaRemota);
    $producto = new Producto("", $nombre, "", $precio,$rutaRemota);
    $producto -> insertar();
    $administrador = new Administrador($_SESSION["id"]);
    $administrador -> consultar();
    $Log = new Log("","Administrador", $hora, $fecha, $administrador -> getCorreo(),"Creo Producto: " . $nombre, "");
    $Log->insertarAdmin($administrador -> getIdAdmin());
}


?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-danger">
					<h4>Crear Producto</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						 Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $nombre ?>" required>
						</div>
						<div class="form-group">
							<label>Precio</label> 
							<input type="number" name="precio" class="form-control" min="1" value="<?php echo $precio ?>" required>
						</div>
						<div class="form-group">
							<label>Imagen</label> 
							<input type="file" name="imagen" class="form-control"value="<?php echo $imagen ?>">
						</div>
						<button type="submit" name="crear" class="btn btn-danger">Crear</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>
<?php
$proveedor = new Proveedor($_SESSION["id"]);
$proveedor -> consultar();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-danger">
					<h4>Proveedor</h4>
				</div>
              	<div class="card-body">
              		<div class="row">
              			<div class="col-9">
							<table class="table table-hover">
								<tr>
									<th>Contacto</th>
									<td><?php echo $proveedor -> getContacto() ?></td>
								</tr>
								<tr>
									<th>Direccion</th>
									<td><?php echo $proveedor -> getDireccion() ?></td>
								</tr>
								<tr>
									<th>Correo</th>
									<td><?php echo $proveedor -> getCorreo() ?></td>
								</tr>
							</table>
						</div>              		
              		</div>              	
            	</div>
            </div>
		</div>
	</div>
</div>
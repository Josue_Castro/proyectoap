<?php 


if(!isset($_SESSION['canasta'])){
    header("Location: index.php?pid=" . base64_encode("presentacion/cliente/catalogo.php"));
}
$a = $_SESSION['canasta'];


?>
<div class="container mt-3">
<div class="form-group">

                    <div class="form-group">
                      <label for="c_diff_country" class="text-black">Pais <span class="text-danger">*</span></label>
                      <select id="c_diff_country" class="form-control">
                        <option value="1">Seleccionar Pais</option>    
                        <option value="2">Argentina</option>    
                        <option value="3">Algeria</option>    
                        <option value="4">Alemania</option>    
                        <option value="5">Ecuador</option>    
                        <option value="6">Espa�a</option>    
                        <option value="7">Francia</option>    
                        <option value="8">Colombia</option>    
                        <option value="9">EE.UU</option>    
                      </select>
                    </div>


                    <div class="form-group row">
                      <div class="col-md-6">
                        <label for="c_diff_fname" class="text-black">Nombre <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="c_diff_fname" name="c_diff_fname">
                      </div>
                      <div class="col-md-6">
                        <label for="c_diff_lname" class="text-black">Apellido <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="c_diff_lname" name="c_diff_lname">
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-12">
                        <label for="c_diff_address" class="text-black">Direccion <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="c_diff_address" name="c_diff_address" placeholder="Direccion">
                      </div>
                    </div>

                    <div class="form-group row mb-5">
                      <div class="col-md-6">
                        <label for="c_diff_email_address" class="text-black">Correo Electronico <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="c_diff_email_address" name="c_diff_email_address">
                      </div>
                      <div class="col-md-6">
                        <label for="c_diff_phone" class="text-black">Celular <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="c_diff_phone" name="c_diff_phone" placeholder="Numero de Telefono">
                      </div>
                    </div>

                  </div>

                </div>
              </div>
</div>
<div class="container mt-3">
 <div class="row mb-5">
              <div class="col-md-12">
                <h2 class="h3 mb-3 text-black text-center">Tu Pedido</h2>
                <div class="p-3 p-lg-5 border">
                  <table class="table site-block-order-table mb-5">
                    <thead>
                      <th>Producto</th>
                      <th>Total</th>
                    </thead>
                    <tbody>
                    <?php 
                    $total=0;
                    for($i=0; $i<count($a);$i++){
                        $total= $total + ($a[$i]['precio'] * $a[$i]['cantidad']);
                        
                    ?>
                      <tr>
                        <td><?php echo $a[$i]['nombre']?> x <?php echo $a[$i]['cantidad'] ?> Unidades</td>
                        <td>$<?php echo $a[$i]['precio']?></td>
                      </tr>
                      
                      <?php 
                        }
                      ?>
                      <tr>
                        <td>Total</td>
                        <td>$<?php echo $total?></td>
                      </tr>
                    </tbody>
                  </table>

                  <div class="form-group">
                    <button class="btn btn-danger btn-lg py-3 btn-block" onclick="window.location= 'index.php?pid=<?php echo base64_encode("presentacion/Compra.php") ?>'">Realizar Compra</button>
                  </div>

                </div>
              </div>
            </div>
</div>
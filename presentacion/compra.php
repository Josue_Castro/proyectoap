<?php 

//require_once 'persistencia/Conexion.php';

$a = $_SESSION['canasta'];
$cliente = new Cliente($_SESSION["id"]);
$producto= new Producto();

if(!isset($_SESSION['canasta'])){
    header("Location: index.php?pid=" . base64_encode("presentacion/cliente/catalogo.php"));
}

$a = $_SESSION['canasta'];
$idFactura = rand(100, 1000);
$total = 0;
for($i=0;$i<count($a);$i++){
    
    $prod = new Producto($a[$i]['Id']);
    $prod -> consultar();
    
    $inv=$prod -> getCantidad() - $a[$i]['cantidad'];
    
    $producto = new Producto($a[$i]['Id'],"",$inv,"","");
    $producto -> provCant();
    
    $total = $total+($a[$i]['precio'] * $a[$i]['cantidad']);
}
$cli = $cliente ->getIdCliente();
$fecha = date('Y-m-d');
$hora = date('H:i:s');
$producto -> insertarFactura($idFactura,$fecha,$total,$cli);
$idFactura_p = rand(100, 1000);

for($i=0;$i<count($a);$i++){
    
    $cliente -> consultar();
    $producto -> insertarFactura_p($idFactura_p,$a[$i]['cantidad'],$a[$i]['precio'],$idFactura,$a[$i]['Id']);
    $Log = new Log("","Cliente", $hora, $fecha, $cliente -> getCorreo(),"Realizo compra de: ". $a[$i]['nombre'] , "");
    $Log->insertarCliente($cli);
    
}


unset($_SESSION['canasta']);

?>

<body>
<div class="container md-2">
  <div class="site-wrap"> 
    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <span class="icon-check_circle display-3 text-success"></span>
            <h2 class="display-3 text-black">Gracias</h2>
            <p class="lead mb-5">Su compra a sido realizada</p>
            <p><a href="<?php "Location: index.php?pid=" . base64_encode("presentacion/sesionCliente.php")?>" class="btn btn-sm btn-danger">Volver a la Tienda</a></p>
          </div>
        </div>
      </div>
    </div> 
  </div>
</div>
</body>
<?php

$log = new Log();
$LogsA = $log -> consultarTodosLogAdmin();
$LogsC = $log -> consultarTodosLogCli();
$LogsP = $log -> consultarTodosLogProv();

?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-danger">
					<h4>Log</h4>
				</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Actor</th>
							<th>Hora</th>
							<th>Fecha</th>
							<th>datos</th>
							<th>accion</th>
							<th></th>
						</tr>
						<?php
						$i=1;
						
						foreach($LogsA as $LogActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $LogActual -> getActor() . "</td>";
						    echo "<td>" . $LogActual -> getHora() . "</td>";
						    echo "<td>" . $LogActual -> getFecha() . "</td>";
						    echo "<td>" . $LogActual -> getDatos() . "</td>";
						    echo "<td>" . $LogActual -> getAccion() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
						<?php

						foreach($LogsC as $LogActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $LogActual -> getActor() . "</td>";
						    echo "<td>" . $LogActual -> getHora() . "</td>";
						    echo "<td>" . $LogActual -> getFecha() . "</td>";
						    echo "<td>" . $LogActual -> getDatos() . "</td>";
						    echo "<td>" . $LogActual -> getAccion() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
						<?php

						foreach($LogsP as $LogActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $LogActual -> getActor() . "</td>";
						    echo "<td>" . $LogActual -> getHora() . "</td>";
						    echo "<td>" . $LogActual -> getFecha() . "</td>";
						    echo "<td>" . $LogActual -> getDatos() . "</td>";
						    echo "<td>" . $LogActual -> getAccion() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>
<?php
$correo = $_POST["correo"];
$clave = $_POST["clave"];
$administrador = new Administrador("", "", "", $correo, $clave);
$cliente = new Cliente("", "", "","", $correo, $clave);
$proveedor = new Proveedor("","", "",$correo,$clave,"");
$hora = date("h:i:s");
$fecha = date("Y-m-d");

if($administrador -> autenticar()){
    
    $_SESSION["id"] = $administrador -> getIdAdmin();
    $_SESSION["rol"] = "Administrador";
    $Log = new Log("","Administrador", $hora, $fecha, $administrador -> getCorreo(),"Inicio de Sesion", "");
    $Log->insertarAdmin($administrador -> getIdAdmin());
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionAdministrador.php"));
    
}else if($cliente -> autenticar()){
    if($cliente -> getEstado() == -1){
        header("Location: index.php?error=2");
    }else if($cliente -> getEstado() == 0){
        header("Location: index.php?error=3");        
    }else{
        $_SESSION["id"] = $cliente -> getIdCliente();
        $_SESSION["rol"] = "Cliente";
        $Log = new Log("","Cliente", $hora, $fecha, $cliente -> getCorreo(),"Inicio de Sesion", "");
        $Log->insertarCliente($cliente -> getIdCliente());
        header("Location: index.php?pid=" . base64_encode("presentacion/sesionCliente.php"));
    }
}else if ($proveedor -> autenticar()){

    $_SESSION["id"] = $proveedor -> getIdProveedor();
    $_SESSION["rol"] = "Proveedor";
    $Log = new Log("","Proveedor", $hora, $fecha, $proveedor -> getCorreo(),"Inicio de Sesion", "");
    $Log->insertarProveedor($proveedor -> getIdProveedor());
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionProveedor.php"));
    
}else {
    header("Location: index.php?error=1");
}
?>
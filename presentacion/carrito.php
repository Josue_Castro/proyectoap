<?php

require_once 'persistencia/Conexion.php';

if(isset($_SESSION['canasta'])){
    
    if(isset($_GET["idProducto"])){
        $arreglo = $_SESSION['canasta'];
        $en = false;
        $num = 0;
        
        for($i=0;$i<count($arreglo);$i++){
            if($arreglo[$i]['Id'] == $_GET["idProducto"]){
                $en = true;
                $num =$i;
            }
        }
        
        if($en == true){
            $arreglo[$num]['cantidad']=$arreglo[$num]['cantidad']+1;
            $_SESSION['canasta'] = $arreglo;
        }else{
            $nombre="";
            $precio="";
            $imagen="";
            
            $con = new Conexion();
            
            $con -> abrir();
            $res = $con -> ejecutar('select nombre,precio,imagen from producto where idProducto=' . $_GET["idProducto"]);
            $con -> cerrar();
            $fila = $con -> extraer();
            
            $nombre=$fila[0];
            $precio=$fila[1];
            $imagen=$fila[2];
            
            $arregloNuev= array(
                'Id'=> $_GET["idProducto"],
                'nombre'=>$nombre,
                'precio'=>$precio,
                'imagen'=>$imagen,
                'cantidad'=>1
            );
            
            array_push($arreglo, $arregloNuev);
            $_SESSION['canasta']=$arreglo;
        }
    }
    
            
    
}else if (isset($_GET["idProducto"])){
   
    $nombre="";
    $precio="";
    $imagen="";
    
    $con = new Conexion();
    
    $con -> abrir();
    $res = $con -> ejecutar('select nombre,precio,imagen from producto where idProducto=' . $_GET["idProducto"]);
    $con -> cerrar();
    $fila = $con -> extraer();
    
    $nombre=$fila[0];
    $precio=$fila[1];
    $imagen=$fila[2];
    
    $arreglo[]= array(
        'Id'=> $_GET["idProducto"],
        'nombre'=>$nombre,
        'precio'=>$precio,
        'imagen'=>$imagen,
        'cantidad'=>1
    );
    
    $_SESSION['canasta']=$arreglo;
}



?>
<div class="site-wrap">

    <div class="site-section">
      <div class="container">
        <div class="row mb-5">
          <form class="col-md-12" method="post">
            <div class="site-blocks-table">
              <table class="table table-bordered">
                <thead>
                
                
                  <tr>
                    <th class="imagen">Imagen</th>
                    <th class="nombre">Producto</th>
                    <th class="precio">Precio</th>
                    <th class="cantidad">Cantidad</th>
                    <th class="total">Total</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                $total = 0;
                if(isset($_SESSION['canasta'])){
                    $arregloCarrito = $_SESSION['canasta'];
                    for($i=0;$i<count($arregloCarrito);$i++){
                        $total=$total + ($arregloCarrito[$i]['precio'] * $arregloCarrito[$i]['cantidad']);
                ?>
                  <tr>
                    <td class="imagen">
                      <?php echo (($arregloCarrito[$i]['imagen']!="")?"<img src='" . $arregloCarrito[$i]['imagen'] . "' height='200px'>":"")?>
                    </td>
                    <td class="nombre">
                      <h2 class="h5 text-black"><?php echo $arregloCarrito[$i]['nombre']  ?></h2>
                    </td>
                    <td class="precio"><?php echo $arregloCarrito[$i]['precio']?></td>
                    <td>
                    <div class="input-group mb-3" style="max-width: 120px;">
                        <div class="input-group-prepend">
                          <button class="btn btn-outline-danger js-btn-minus btnIn" type="button">&minus;</button>
                        </div>
                        <input type="text" class="form-control text-center txtCant" 
                        data-precio="<?php echo $arregloCarrito[$i]['precio'] ?>"
                        data-id="<?php echo $arregloCarrito[$i]['Id'] ?>"
                        value="<?php echo $arregloCarrito[$i]['cantidad']?>" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
                        <div class="input-group-append">
                          <button class="btn btn-outline-danger js-btn-plus btnIn" type="button">&plus;</button>
                        </div>
                      </div>
                    </td>
                    <td class="cantidad<?php echo $arregloCarrito[$i]['Id'] ?>">$<?php echo $arregloCarrito[$i]['precio'] * $arregloCarrito[$i]['cantidad']?></td>
                    <td><a href="#" class="btn btn-danger btn-sm btnEliminar" data-id="<?php echo $arregloCarrito[$i]['Id']?>">Eliminar</a></td>
                  </tr>
					<?php 
					
                    }}
					?>
                </tbody>
              </table>
            </div>
          </form>
        </div>

        <div class="row">
          
          <div class="col-md-6 pl-5">
            <div class="row justify-content-end">
              <div class="col-md-7">
                <div class="row">
                  <div class="col-md-12 text-right border-bottom mb-5">
                    <h3 class="text-black h4 text-uppercase">Compra</h3>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-md-6">
                    <span class="text-black">Total</span>
                  </div>
                  <div class="col-md-6 text-right">
                    <strong class="text-black">$<?php echo $total?></strong>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <button class="btn btn-danger btn-lg py-3 btn-block" onclick="window.location= 'index.php?pid=<?php echo base64_encode("presentacion/procesoCompra.php") ?>'">Comprar</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  
<script>

	$(document).ready(function($) {
	
		var sitePlusMinus = function() {
		$('.js-btn-minus').on('click', function(e){
			e.preventDefault();
			if ( $(this).closest('.input-group').find('.form-control').val() != 0  ) {
				$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) - 1);
			} else {
				$(this).closest('.input-group').find('.form-control').val(parseInt(0));
			}
		});
		$('.js-btn-plus').on('click', function(e){
			e.preventDefault();
			$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) + 1);
		});
	};
	sitePlusMinus();
	
	});

</script>
  
<script>
	$(document).ready(function(){
		$(".btnEliminar").click(function(event){
			event.preventDefault();
			var id = $(this).data('id');
			var boton = $(this);
			$.ajax({
				method:'POST',
				url:'./presentacion/eliminarCarrito.php',
				data:{
					id:id
				}			
			}).done(function(respuesta){
				boton.parent('td').parent('tr').remove();
			});	
		});
		$(".txtCant").keyup(function(){
			var cantidad = $(this).val();
			var precio = $(this).data('precio');
			var id = $(this).data('id')
			incrementar(cantidad,precio,id);
		});
		$(".btnIn").click(function(){
			var precio = $(this).parent('div').parent('div').find('input').data('precio');
			var id = $(this).parent('div').parent('div').find('input').data('id');
			var cantidad = $(this).parent('div').parent('div').find('input').val();
			incrementar(cantidad,precio,id);
		});
		function incrementar(cantidad,precio,id){
			var multiplicacion = parseFloat(cantidad) * parseFloat(precio);
			$(".cantidad"+id).text("$"+multiplicacion);
			$.ajax({
				method:'POST',
				url:'./presentacion/actualizarCarrito.php',
				data:{
					id:id,
					cantidad:cantidad
				}			
			}).done(function(respuesta){
				
			});	
		}
	});
</script>


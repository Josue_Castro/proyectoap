<?php 
$error = 0;
$registrado = false;
if(isset($_POST["registrar"])){
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    $clave = $_POST["clave"];
    $cliente = new Cliente("", $nombre, $apellido,"", $correo, $clave);
    if($cliente -> existeCorreo()){
        $error = 1;
    }else{
        $cliente -> registrar();
        $registrado = true;
    }
}
?>

<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-danger">
					<h4>Registro</h4>
				</div>
              	<div class="card-body">
 
        			<form action="index.php?pid=<?php echo base64_encode("presentacion/cliente/registrarCliente.php") ?>" method="post">
        				<div class="form-group">
    						<input name="nombre" type="text" class="form-control" placeholder="nombre" required>
    					</div>
    					<div class="form-group">
    						<input name="apellido" type="text" class="form-control" placeholder="apellido" required>
    					</div>
        				<div class="form-group">
    						<input name="correo" type="email" class="form-control" placeholder="correo" required>
    					</div>
        				<div class="form-group">
    						<input name="clave" type="password" class="form-control" placeholder="clave" required>
    					</div>
        				<div class="form-group">
    						<input name="registrar" type="submit" class="form-control btn btn-danger">
    					</div>	    					
        			</form>     
        			<?php if($error == 1){ ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						El correo <?php echo $correo ?> ya se encuentra registrado.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } else if($registrado) { ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						El cliente fue registrado exitosamente<?php echo $correo ?>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
            	</div>
            </div>		
		</div>
	</div>
</div>
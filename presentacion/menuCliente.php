<?php
$cliente = new Cliente($_SESSION["id"]);
$cliente -> consultar();


$items = 1;
if($cliente -> getNombre() != ""){
    $items++;
}
if($cliente -> getApellido() != ""){
    $items++;
}
if($cliente -> getFoto() != ""){
    $items++;
}
$porcentaje = $items/4 * 100;

$producto = new Producto();
$producto -> consultarTodos();

?>
<nav class="navbar navbar-expand-md navbar-dark bg-danger">
	<a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/sesionCliente.php") ?>"><i class="fas fa-gamepad"></i></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div>
	<?php echo "<td><a href='index.php?pid=". base64_encode("presentacion/carrito.php") . "' data-toggle='tooltip' data-placement='left' title='Carrito'><span class='fas fa-shopping-cart'></span></a></td>";?>
	<span class="count text-white"><a>
		<?php 
    		if(isset($_SESSION['canasta'])){
    		    echo count($_SESSION['canasta']);
    		}else{
    		  echo 0;    
    		}
		?>
	</a></span>
	</div>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Productos</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/cliente/catalogo.php") ?>">Catalogo</a>
					
				</div></li>
		</ul>
		
		 
		
		<ul class="navbar-nav">
		
			<li class="nav-item dropdown active"><a class="nav-link dropdown-toggle"
				href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
	            
	              
				
				aria-haspopup="true" aria-expanded="false"><span class="badge badge-danger"><?php echo $porcentaje . "%"?></span>
				Cliente: <?php echo ($cliente -> getNombre()!=""?$cliente -> getNombre():$cliente -> getCorreo()) ?> <?php echo $cliente -> getApellido() ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown"> 
					<a class="dropdown-item" href="#">Editar Foto</a>
					<a class="dropdown-item" href="#">Cambiar Clave</a>						
				</div></li>
			<li class="nav-item active"><a class="nav-link" href="index.php?cerrarSesion=true">Cerrar Sesion</a></li>
		</ul>
	</div>
</nav>

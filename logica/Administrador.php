<?php 

require_once "persistencia/Conexion.php";
require_once "persistencia/AdministradorDAO.php";

class Administrador{
    
    private $idAdmin;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $conexion;
    private $administradorDAO;
    

    public function getIdAdmin(){
        return $this->idAdmin;
    }

    public function getNombre(){
        return $this->nombre;
    }


    public function getApellido(){
        return $this->apellido;
    }


    public function getCorreo(){
        return $this->correo;
    }

    public function getClave(){
        return $this->clave;
    }

    public function getFoto(){
        return $this->foto;
    } 

    public function Administrador($idAdmin="", $nombre="", $apellido="", $correo="", $clave="", $foto=""){
        $this->idAdmin=$idAdmin;
        $this->nombre=$nombre;
        $this->apellido=$apellido;
        $this->correo=$correo;
        $this->clave=$clave;
        $this->foto=$foto;
        $this->conexion= new Conexion();
        $this->administradorDAO = new AdministradorDAO($this->idAdmin, $this->nombre, $this->apellido, $this->correo, $this->clave, $this->foto);
    }
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idAdmin = $resultado[0];
            return true;
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto = $resultado[3];
    }
    
    
    
    
    
    
    
}





?>
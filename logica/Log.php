<?php 

require_once 'persistencia/Conexion.php';
require_once 'persistencia/LogDAO.php';

class Log{
    
    private $idLog;
    private $actor;
    private $hora;
    private $fecha;
    private $datos;
    private $accion;
    private $conexion;
    private $logDAO;

    public function getIdLog()
    {
        return $this->idLog;
    }

    public function getActor()
    {
        return $this->actor;
    }

    public function getHora()
    {
        return $this->hora;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function getDatos()
    {
        return $this->datos;
    }

    public function getAccion()
    {
        return $this->accion;
    }

    public function Log($idLog="", $actor="", $hora="", $fecha="", $datos="", $accion=""){
        $this->idLog=$idLog;
        $this->actor=$actor;
        $this->hora=$hora;
        $this->fecha=$fecha;
        $this->datos=$datos;
        $this->accion=$accion;
        $this->conexion= new Conexion();
        $this->logDAO= new LogDAO($this->idLog, $this->actor, $this->hora, $this->fecha, $this->datos, $this->accion);
    }
    
    
    public function insertarAdmin($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> insertarAdmin($id));
        $this -> conexion -> cerrar();
    }
    
    public function insertarCliente($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> insertarCliente($id));
        $this -> conexion -> cerrar();
    }
    
    public function insertarProveedor($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> insertarProveedor($id));
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodosLogAdmin(){
        $this -> conexion -> abrir();
        // echo $this -> productoDAO -> consultarTodos();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarTodosLogAdmin());
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
    
    public function consultarTodosLogCli(){
        $this -> conexion -> abrir();
        // echo $this -> productoDAO -> consultarTodos();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarTodosLogCli());
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
    
    public function consultarTodosLogProv(){
        $this -> conexion -> abrir();
        // echo $this -> productoDAO -> consultarTodos();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarTodosLogProv());
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
    
    public function consultarFiltroAdmin($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltroAdmin($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
    
    public function consultarFiltroCli($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltroCli($filtro));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarFiltroProv($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltroProv($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
    
    
}






?>
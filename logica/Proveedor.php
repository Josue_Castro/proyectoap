<?php 

require_once 'persistencia/Conexion.php';
require_once 'persistencia/ProveedorDAO.php';

class Proveedor{
    
    private $idProveedor;
    private $contacto;
    private $Direccion;
    private $correo;
    private $clave;
    private $estado;
    private $conexion;
    private $proveedorDAO;

    public function getIdProveedor()
    {
        return $this->idProveedor;
    }

    public function getContacto()
    {
        return $this->contacto;
    }

    public function getDireccion()
    {
        return $this->Direccion;
    }
    
    public function getCorreo()
    {
        return $this->correo;
    }
    
    public function getClave(){
        return $this->clave;
    }
    
    public function getEstado(){
        return $this->estado;
    }

    public function Proveedor($idProveedor="", $contacto="", $Direccion="", $correo="", $clave="", $estado=""){
         $this->idProveedor=$idProveedor;
         $this->contacto=$contacto;
         $this->Direccion=$Direccion;
         $this->correo=$correo;
         $this->clave=$clave;
         $this->estado=$estado;
         $this->conexion= new Conexion();
         $this->proveedorDAO= new ProveedorDAO($this->idProveedor, $this->contacto, $this->Direccion, $this->correo, $this->clave, $this->estado);
    }
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idProveedor = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else {
            return false;
        }
    }
            
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> contacto = $resultado[0];
        $this -> Direccion = $resultado[1];
        $this -> correo = $resultado[2];
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> consultarTodos());
        $proveedores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Proveedor($resultado[0], $resultado[1], $resultado[2], $resultado[3], "", $resultado[4]);
            array_push($proveedores, $p);
        }
        $this -> conexion -> cerrar();
        return $proveedores;
    }
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    
    public function ingresarProd_Prov($idProv, $idProd){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> ingresarProd_Prov($idProv, $idProd));
        $this -> conexion -> cerrar();
    }
    
    public function cambiarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> cambiarEstado());
        $this -> conexion -> cerrar();
    }
    
}




?>
<?php 

require_once "persistencia/Conexion.php";
require_once "persistencia/ProductoDAO.php";

class Producto{
    
    private $idProducto;
    private $nombre;
    private $cantidad;
    private $precio;
    private $imagen;
    private $conexion;
    private $productoDAO;
    
    public function getIdProducto(){
        return $this->idProducto;
    }
    
    public function getNombre(){
        return $this->nombre;
    }
    
    public function getCantidad(){
        return $this->cantidad;
    }
    
    public function getPrecio(){
        return $this->precio;
    }
    
    public function getImagen(){
        return $this -> imagen;
    }
    
    
    public function Producto($idProducto="", $nombre="", $cantidad="", $precio="", $imagen=""){
        $this->idProducto=$idProducto;
        $this->nombre=$nombre;
        $this->cantidad=$cantidad;
        $this->precio=$precio;
        $this->imagen=$imagen;
        $this->conexion= new Conexion();
        $this->productoDAO= new ProductoDAO($this->idProducto, $this->nombre, $this->cantidad, $this->precio, $this->imagen);
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        //echo $this -> productoDAO -> consultar();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> cantidad = $resultado[1];
        $this -> precio = $resultado[2];
        $this -> imagen = $resultado[3];
    }
   
    
    public function insertar(){
        $this -> conexion -> abrir();
        //echo $this -> productoDAO -> insertar();
        $this -> conexion -> ejecutar($this -> productoDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    
    public function insertarFactura($id,$fecha,$valor,$idCliente){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> insertarFactura($id,$fecha,$valor,$idCliente));
        $this -> conexion -> cerrar();
    }
    
    public function insertarFactura_p($id,$cantidad,$precio,$FidFactura, $FidProducto){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> insertarFactura_p($id,$cantidad,$precio,$FidFactura, $FidProducto));
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
       // echo $this -> productoDAO -> consultarTodos();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTodos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarPaginacion($cantidad, $pagina));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> editar());
        $this -> conexion -> cerrar();
    }
    
    public function provCant(){
        $this -> conexion -> abrir();
        //echo $this -> productoDAO -> provCant();
        $this -> conexion -> ejecutar($this -> productoDAO -> provCant());
        $this -> conexion -> cerrar();
    }

    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarFiltro($filtro));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    

    
}









?>
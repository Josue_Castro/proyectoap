-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-07-2020 a las 17:47:12
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyectoap`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idAdministrador` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `clave` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `foto` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idAdministrador`, `nombre`, `apellido`, `correo`, `clave`, `foto`) VALUES
(1, 'Josue', 'Castro', 'josue06castp@gmail.com', 'd127f35421d28acc999a03607016c0bc', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `clave` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `foto` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `nombre`, `apellido`, `estado`, `correo`, `clave`, `foto`) VALUES
(1, 'Carlos', 'Martinez', 1, 'carlos@gmail.com', '202cb962ac59075b964b07152d234b70', NULL),
(2, 'Camila', 'Rodriguez', 1, 'camila@gmail.com', '68053af2923e00204c3ca7c6a3150cf7', NULL),
(3, 'Esteban', 'Castro', 1, 'esteban@gmail.com', '202cb962ac59075b964b07152d234b70', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `idFactura` int(100) NOT NULL,
  `fecha` date DEFAULT NULL,
  `valor` int(100) DEFAULT NULL,
  `Cliente_idCliente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`idFactura`, `fecha`, `valor`, `Cliente_idCliente`) VALUES
(192, '2020-07-14', 200000, 1),
(298, '2020-07-15', 843000, 1),
(307, '2020-07-15', 358000, 1),
(328, '2020-07-14', 50000, 1),
(359, '2020-07-15', 600000, 1),
(372, '2020-07-14', 123000, 1),
(480, '2020-07-15', 170000, 1),
(499, '2020-07-14', 30000, 1),
(502, '2020-07-14', 73000, 1),
(526, '2020-07-14', 140000, 1),
(601, '2020-07-15', 685000, 2),
(636, '2020-07-15', 73000, 3),
(656, '2020-07-14', 73000, 1),
(680, '2020-07-14', 16000, 1),
(780, '2020-07-14', 50000, 1),
(885, '2020-07-15', 70000, 1),
(906, '2020-07-14', 140000, 1),
(941, '2020-07-15', 300000, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_producto`
--

CREATE TABLE `factura_producto` (
  `idFactura_Producto` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` int(11) DEFAULT NULL,
  `Factura_idFactura` int(11) DEFAULT NULL,
  `Producto_idProducto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `factura_producto`
--

INSERT INTO `factura_producto` (`idFactura_Producto`, `cantidad`, `precio`, `Factura_idFactura`, `Producto_idProducto`) VALUES
(121, 2, 85000, 480, 25),
(123, 1, 50000, 372, 3),
(129, 2, 300000, 359, 3),
(160, 1, 70000, 885, 7),
(189, 1, 200000, 307, 21),
(327, 1, 73000, 656, 19),
(350, 1, 200000, 192, 21),
(495, 2, 70000, 906, 7),
(546, 2, 300000, 298, 3),
(590, 1, 73000, 502, 19),
(610, 1, 50000, 780, 3),
(719, 1, 85000, 601, 25),
(778, 1, 16000, 680, 5),
(838, 1, 73000, 636, 19),
(876, 2, 70000, 526, 7),
(889, 1, 300000, 941, 3),
(952, 1, 30000, 499, 6),
(991, 1, 50000, 328, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logadministrador`
--

CREATE TABLE `logadministrador` (
  `idLogAdministrador` int(11) NOT NULL,
  `actor` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `datos` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `accion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Administrador_idAdministrador` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `logadministrador`
--

INSERT INTO `logadministrador` (`idLogAdministrador`, `actor`, `hora`, `fecha`, `datos`, `accion`, `Administrador_idAdministrador`) VALUES
(20, 'Administrador', '03:08:42', '2020-07-14', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(21, 'Administrador', '03:44:39', '2020-07-14', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(26, 'Administrador', '03:50:01', '2020-07-14', 'josue06castp@gmail.com', 'Creo Producto: Gears of War 5', 1),
(29, 'Administrador', '03:53:20', '2020-07-14', 'josue06castp@gmail.com', 'Edito Producto: Gears of War 5', 1),
(30, 'Administrador', '06:05:36', '2020-07-14', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(31, 'Administrador', '06:36:12', '2020-07-14', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(32, 'Administrador', '06:37:12', '2020-07-14', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(44, 'Administrador', '11:34:20', '2020-07-14', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(45, 'Administrador', '12:05:24', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(47, 'Administrador', '12:27:27', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(48, 'Administrador', '12:27:44', '2020-07-15', 'josue06castp@gmail.com', 'Creo Producto: FIFA 20', 1),
(49, 'Administrador', '12:29:40', '2020-07-15', 'josue06castp@gmail.com', 'Edito Producto: FIFA 20', 1),
(55, 'Administrador', '12:57:32', '2020-07-15', 'josue06castp@gmail.com', 'Creo Producto: FIFA 19', 1),
(56, 'Administrador', '12:57:46', '2020-07-15', 'josue06castp@gmail.com', 'Edito Producto: FIFA 19', 1),
(57, 'Administrador', '01:16:02', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(58, 'Administrador', '01:49:58', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(59, 'Administrador', '02:06:11', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(64, 'Administrador', '02:21:04', '2020-07-15', 'josue06castp@gmail.com', 'Creo Producto: ARK Survival Evolved', 1),
(65, 'Administrador', '03:11:37', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(66, 'Administrador', '03:37:33', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(67, 'Administrador', '03:54:30', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(68, 'Administrador', '04:15:17', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(69, 'Administrador', '04:50:21', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(70, 'Administrador', '04:51:55', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(71, 'Administrador', '05:05:09', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(72, 'Administrador', '06:05:56', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(73, 'Administrador', '06:07:46', '2020-07-15', 'josue06castp@gmail.com', 'Creo Producto: PES 20', 1),
(74, 'Administrador', '06:08:22', '2020-07-15', 'josue06castp@gmail.com', 'Edito Producto: COD Modern Warfare', 1),
(75, 'Administrador', '06:19:21', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(76, 'Administrador', '06:20:28', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1),
(77, 'Administrador', '05:26:14', '2020-07-15', 'josue06castp@gmail.com', 'Inicio de Sesion', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logcliente`
--

CREATE TABLE `logcliente` (
  `idLogCliente` int(11) NOT NULL,
  `actor` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `datos` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `accion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Cliente_idCliente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `logcliente`
--

INSERT INTO `logcliente` (`idLogCliente`, `actor`, `hora`, `fecha`, `datos`, `accion`, `Cliente_idCliente`) VALUES
(9, 'Cliente', '07:32:55', '2020-07-14', 'carlos@gmail.com', 'Inicio de Sesion', 1),
(17, 'Cliente', '09:04:43', '2020-07-14', 'carlos@gmail.com', 'Inicio de Sesion', 1),
(21, 'Cliente', '03:12:12', '2020-07-14', 'carlos@gmail.com', 'Inicio de Sesion', 1),
(24, 'Cliente', '03:39:31', '2020-07-14', 'carlos@gmail.com', 'Inicio de Sesion', 1),
(25, 'Cliente', '15:40:16', '2020-07-14', 'carlos@gmail.com', 'Realizo compra de: The Last of Us 2', 1),
(26, 'Cliente', '15:40:16', '2020-07-14', 'carlos@gmail.com', 'Realizo compra de: Halo Master Collection', 1),
(34, 'Cliente', '04:10:02', '2020-07-14', 'carlos@gmail.com', 'Inicio de Sesion', 1),
(35, 'Cliente', '04:10:17', '2020-07-14', 'carlos@gmail.com', 'Inicio de Sesion', 1),
(36, 'Cliente', '16:13:30', '2020-07-14', 'carlos@gmail.com', 'Realizo compra de: Gears of War 5', 1),
(37, 'Cliente', '05:46:51', '2020-07-14', 'carlos@gmail.com', 'Inicio de Sesion', 1),
(38, 'Cliente', '06:36:59', '2020-07-14', 'carlos@gmail.com', 'Inicio de Sesion', 1),
(39, 'Cliente', '12:09:37', '2020-07-15', 'carlos@gmail.com', 'Inicio de Sesion', 1),
(40, 'Cliente', '02:00:23', '2020-07-15', 'carlos@gmail.com', 'Inicio de Sesion', 1),
(41, 'Cliente', '05:10:07', '2020-07-15', 'carlos@gmail.com', 'Inicio de Sesion', 1),
(42, 'Cliente', '05:11:38', '2020-07-15', 'carlos@gmail.com', 'Realizo compra de: Gears of War 5', 1),
(43, 'Cliente', '05:11:38', '2020-07-15', 'carlos@gmail.com', 'Realizo compra de: Halo Master Collection', 1),
(44, 'Cliente', '05:11:38', '2020-07-15', 'carlos@gmail.com', 'Realizo compra de: FIFA 20', 1),
(45, 'Cliente', '05:27:04', '2020-07-15', 'carlos@gmail.com', 'Realizo compra de: The Last of Us 2', 1),
(46, 'Cliente', '05:27:04', '2020-07-15', 'carlos@gmail.com', 'Realizo compra de: Halo Master Collection', 1),
(47, 'Cliente', '05:27:04', '2020-07-15', 'carlos@gmail.com', 'Realizo compra de: FIFA 20', 1),
(48, 'Cliente', '05:29:41', '2020-07-15', 'carlos@gmail.com', 'Inicio de Sesion', 1),
(49, 'Cliente', '05:30:39', '2020-07-15', 'carlos@gmail.com', 'Realizo compra de: FIFA 20', 1),
(50, 'Cliente', '05:56:25', '2020-07-15', 'carlos@gmail.com', 'Realizo compra de: COD Modern Warfare', 1),
(51, 'Cliente', '06:00:04', '2020-07-15', 'carlos@gmail.com', 'Realizo compra de: The Last of Us 2', 1),
(52, 'Cliente', '06:01:22', '2020-07-15', 'camila@gmail.com', 'Inicio de Sesion', 2),
(53, 'Cliente', '06:02:26', '2020-07-15', 'camila@gmail.com', 'Realizo compra de: The Last of Us 2', 2),
(54, 'Cliente', '06:11:13', '2020-07-15', 'camila@gmail.com', 'Inicio de Sesion', 2),
(55, 'Cliente', '06:12:38', '2020-07-15', 'camila@gmail.com', 'Realizo compra de: FIFA 20', 2),
(56, 'Cliente', '06:12:38', '2020-07-15', 'camila@gmail.com', 'Realizo compra de: The Last of Us 2', 2),
(57, 'Cliente', '06:14:09', '2020-07-15', 'carlos@gmail.com', 'Inicio de Sesion', 1),
(58, 'Cliente', '06:19:40', '2020-07-15', 'esteban@gmail.com', 'Inicio de Sesion', 3),
(59, 'Cliente', '06:20:20', '2020-07-15', 'esteban@gmail.com', 'Realizo compra de: Halo Master Collection', 3),
(60, 'Cliente', '05:17:09', '2020-07-15', 'carlos@gmail.com', 'Inicio de Sesion', 1),
(61, 'Cliente', '05:30:42', '2020-07-15', 'carlos@gmail.com', 'Inicio de Sesion', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logproveedor`
--

CREATE TABLE `logproveedor` (
  `idLogProveedor` int(11) NOT NULL,
  `actor` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `datos` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `accion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Proveedor_idProveedor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `logproveedor`
--

INSERT INTO `logproveedor` (`idLogProveedor`, `actor`, `hora`, `fecha`, `datos`, `accion`, `Proveedor_idProveedor`) VALUES
(2, 'Proveedor', '09:04:00', '2020-07-14', 'rx@games.com', 'Inicio de Sesion', 5),
(3, 'Proveedor', '09:04:24', '2020-07-14', 'ps@games.com', 'Inicio de Sesion', 4),
(4, 'Proveedor', '03:54:42', '2020-07-14', 'rx@games.com', 'Inicio de Sesion', 5),
(5, 'Proveedor', '04:05:12', '2020-07-14', 'ps@games.com', 'Inicio de Sesion', 4),
(6, 'Proveedor', '04:08:02', '2020-07-14', 'ps@games.com', 'Proveer Producto: COD 4 Modern Warfare', 4),
(7, 'Proveedor', '12:58:07', '2020-07-15', 'ps@games.com', 'Inicio de Sesion', 4),
(8, 'Proveedor', '12:58:14', '2020-07-15', 'ps@games.com', 'Proveer Producto: FIFA 20', 4),
(9, 'Proveedor', '12:58:21', '2020-07-15', 'ps@games.com', 'Proveer Producto: FIFA 19', 4),
(10, 'Proveedor', '04:16:57', '2020-07-15', 'ps@games.com', 'Inicio de Sesion', 4),
(11, 'Proveedor', '04:50:00', '2020-07-15', 'ps@games.com', 'Inicio de Sesion', 4),
(12, 'Proveedor', '05:28:47', '2020-07-15', 'ps@games.com', 'Inicio de Sesion', 4),
(13, 'Proveedor', '05:29:10', '2020-07-15', 'ps@games.com', 'Proveer Producto: ARK Survival Evolved', 4),
(14, 'Proveedor', '06:10:37', '2020-07-15', 'ps@games.com', 'Inicio de Sesion', 4),
(15, 'Proveedor', '06:10:51', '2020-07-15', 'ps@games.com', 'Proveer Producto: PES 20', 4),
(16, 'Proveedor', '05:16:33', '2020-07-15', 'ps@games.com', 'Inicio de Sesion', 4),
(17, 'Proveedor', '05:30:23', '2020-07-15', 'ps@games.com', 'Inicio de Sesion', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idProducto` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` int(45) DEFAULT NULL,
  `imagen` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idProducto`, `nombre`, `cantidad`, `precio`, `imagen`) VALUES
(3, 'The Last of Us 2', 51, 300000, 'imagenes/1594421601.jpg'),
(5, 'Uncharted 2', 16, 16000, 'imagenes/1594421841.jpg'),
(6, 'COD 4 Modern Warfare', 31, 30000, 'imagenes/1594447887.jpg'),
(7, 'COD Modern Warfare', 22, 100000, 'imagenes/1594448000.jpg'),
(19, 'Halo Master Collection', 28, 73000, 'imagenes/1594706672.jpg'),
(21, 'Gears of War 5', 38, 200000, 'imagenes/1594734800.jpg'),
(25, 'FIFA 20', 24, 85000, 'imagenes/1594765780.jpg'),
(29, 'FIFA 19', 20, 65000, 'imagenes/1594767466.jpg'),
(34, 'ARK Survival Evolved', 20, 75000, 'imagenes/1594772464.jpg'),
(35, 'PES 20', 50, 55000, 'imagenes/1594786066.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_proveedor`
--

CREATE TABLE `producto_proveedor` (
  `idProducto_Proveedor` int(11) NOT NULL,
  `Proveedor_idProveedor` int(11) DEFAULT NULL,
  `Producto_idProducto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `producto_proveedor`
--

INSERT INTO `producto_proveedor` (`idProducto_Proveedor`, `Proveedor_idProveedor`, `Producto_idProducto`) VALUES
(1, 4, 3),
(2, 4, 5),
(3, 4, 6),
(4, 4, 7),
(5, 4, 19),
(11, 5, 21),
(13, 5, 7),
(14, 5, 5),
(15, 5, 3),
(16, 4, 19),
(17, 4, 5),
(18, 4, 6),
(19, 4, 25),
(20, 4, 29),
(21, 4, 34),
(22, 4, 35);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `idProveedor` int(11) NOT NULL,
  `contacto` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `clave` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`idProveedor`, `contacto`, `direccion`, `correo`, `clave`, `estado`) VALUES
(4, 'PlayStationGames', 'Cll 100 #87', 'ps@games.com', 'caf1a3dfb505ffed0d024130f58c5cfa', 1),
(5, 'RXGames', 'Cll 80 #47', 'rx@games.com', '250cf8b51c773f3f8dc8b4be867a9a02', 0),
(6, 'ZGames', 'Cll 80 #54', 'zg@games.com', '202cb962ac59075b964b07152d234b70', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idAdministrador`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`idFactura`),
  ADD KEY `Cliente_idCliente` (`Cliente_idCliente`);

--
-- Indices de la tabla `factura_producto`
--
ALTER TABLE `factura_producto`
  ADD PRIMARY KEY (`idFactura_Producto`),
  ADD KEY `Factura_idProducto` (`Factura_idFactura`),
  ADD KEY `Producto_idProducto` (`Producto_idProducto`);

--
-- Indices de la tabla `logadministrador`
--
ALTER TABLE `logadministrador`
  ADD PRIMARY KEY (`idLogAdministrador`),
  ADD KEY `Administrador_idAdministrador` (`Administrador_idAdministrador`);

--
-- Indices de la tabla `logcliente`
--
ALTER TABLE `logcliente`
  ADD PRIMARY KEY (`idLogCliente`),
  ADD KEY `Cliente_idCliente` (`Cliente_idCliente`);

--
-- Indices de la tabla `logproveedor`
--
ALTER TABLE `logproveedor`
  ADD PRIMARY KEY (`idLogProveedor`),
  ADD KEY `Proveedor_idProveedor` (`Proveedor_idProveedor`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `producto_proveedor`
--
ALTER TABLE `producto_proveedor`
  ADD PRIMARY KEY (`idProducto_Proveedor`),
  ADD KEY `Proveedor_idProveedor` (`Proveedor_idProveedor`),
  ADD KEY `Producto_idProducto` (`Producto_idProducto`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idProveedor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `logadministrador`
--
ALTER TABLE `logadministrador`
  MODIFY `idLogAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT de la tabla `logcliente`
--
ALTER TABLE `logcliente`
  MODIFY `idLogCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT de la tabla `logproveedor`
--
ALTER TABLE `logproveedor`
  MODIFY `idLogProveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `producto_proveedor`
--
ALTER TABLE `producto_proveedor`
  MODIFY `idProducto_Proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idProveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `cliente` (`idCliente`);

--
-- Filtros para la tabla `factura_producto`
--
ALTER TABLE `factura_producto`
  ADD CONSTRAINT `factura_producto_ibfk_1` FOREIGN KEY (`Factura_idFactura`) REFERENCES `factura` (`idFactura`),
  ADD CONSTRAINT `factura_producto_ibfk_2` FOREIGN KEY (`Producto_idProducto`) REFERENCES `producto` (`idProducto`);

--
-- Filtros para la tabla `logadministrador`
--
ALTER TABLE `logadministrador`
  ADD CONSTRAINT `logadministrador_ibfk_1` FOREIGN KEY (`Administrador_idAdministrador`) REFERENCES `administrador` (`idAdministrador`);

--
-- Filtros para la tabla `logcliente`
--
ALTER TABLE `logcliente`
  ADD CONSTRAINT `logcliente_ibfk_1` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `cliente` (`idCliente`);

--
-- Filtros para la tabla `logproveedor`
--
ALTER TABLE `logproveedor`
  ADD CONSTRAINT `logproveedor_ibfk_1` FOREIGN KEY (`Proveedor_idProveedor`) REFERENCES `proveedor` (`idProveedor`);

--
-- Filtros para la tabla `producto_proveedor`
--
ALTER TABLE `producto_proveedor`
  ADD CONSTRAINT `producto_proveedor_ibfk_1` FOREIGN KEY (`Proveedor_idProveedor`) REFERENCES `proveedor` (`idProveedor`),
  ADD CONSTRAINT `producto_proveedor_ibfk_2` FOREIGN KEY (`Producto_idProducto`) REFERENCES `producto` (`idProducto`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

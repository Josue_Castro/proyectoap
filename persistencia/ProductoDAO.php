<?php 
class ProductoDAO{
    
    private $idProducto;
    private $nombre;
    private $cantidad;
    private $precio;
    private $imagen;

    public function ProductoDAO($idProducto="", $nombre="", $cantidad="", $precio="", $imagen=""){
        $this->idProducto=$idProducto;
        $this->nombre=$nombre;
        $this->cantidad=$cantidad;
        $this->precio=$precio;
        $this->imagen=$imagen;
    }
    
    public function consultar(){
        return "select nombre, cantidad, precio, imagen
                from Producto
                where idProducto = '" . $this -> idProducto .  "'";
    }
    
    public function insertarFactura($id,$fecha,$valor,$idCliente){
        return "insert into Factura (idFactura, fecha, valor, Cliente_idCliente)
                values ('" . $id . "', '" . $fecha . "', '" . $valor . "', '" . $idCliente . "')";
    }
    
    public function insertarFactura_p($id,$cantidad,$precio,$FidFactura, $FidProducto){
        return "insert into Factura_Producto (idFactura_Producto, cantidad, precio, Factura_idFactura, Producto_idProducto)
                values ('" . $id . "', '" . $cantidad . "', '" . $precio . "', '" . $FidFactura . "', '" . $FidProducto . "')";
    }
    
    public function insertar(){
        return "insert into Producto (nombre, cantidad, precio, imagen)
                values ('" . $this -> nombre . "', '" . $this -> cantidad . "', '" . $this -> precio . "', '" . $this -> imagen . "')";
    }
    
    public function consultarTodos(){
        return "select idProducto, nombre, cantidad, precio, imagen
                from Producto";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idProducto, nombre, cantidad, precio, imagen
                from Producto
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(idProducto)
                from Producto";
    }
    
    public function editar(){
        return "update Producto
                set nombre = '" . $this -> nombre . "', precio = '" . $this -> precio . "'" . (($this -> imagen!="")?", imagen = '" . $this -> imagen . "'":"") . "
                where idProducto = '" . $this -> idProducto .  "'";
    }
    
    public function provCant(){
        return "update Producto
                set cantidad = '" . $this->cantidad . "'
                where idProducto = '" . $this -> idProducto .  "'";
    }
    
    
    public function consultarFiltro($filtro){
        return "select idProducto, nombre, cantidad, precio, imagen
                from Producto
                where nombre like '%" . $filtro . "%' or cantidad like '" . $filtro . "%' or precio like '" . $filtro . "%'";
    }
    
    
    
    
}


?>
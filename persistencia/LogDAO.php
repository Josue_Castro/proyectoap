<?php 

class LogDAO{
    
    private $idLog;
    private $actor;
    private $hora;
    private $fecha;
    private $datos;
    private $accion;
    
    public function LogDAO($idLog="", $actor="", $hora="", $fecha="", $datos="", $accion=""){
        $this->idLog=$idLog;
        $this->actor=$actor;
        $this->hora=$hora;
        $this->fecha=$fecha;
        $this->datos=$datos;
        $this->accion=$accion;
    }
    
    public function insertarAdmin($id){
        return "insert into logadministrador (actor, hora, fecha, datos, accion, Administrador_idAdministrador)
                values ('" . $this->actor . "', '" . $this->hora . "', '" . $this->fecha . "', '" . $this->datos . "', '" . $this->accion . "', '" . $id . "')";
    }
    
    public function insertarCliente($id){
        return "insert into logcliente (actor, hora, fecha, datos, accion, Cliente_idCliente)
                values ('" . $this->actor . "', '" . $this->hora . "', '" . $this->fecha . "', '" . $this->datos . "', '" . $this->accion . "', '" . $id . "')";
    }
    
    public function insertarProveedor($id){
        return "insert into logproveedor (actor, hora, fecha, datos, accion, Proveedor_idProveedor)
                values ('" . $this->actor . "', '" . $this->hora . "', '" . $this->fecha . "', '" . $this->datos . "', '" . $this->accion . "', '" . $id . "')";
    }
    
    public function consultarTodosLogAdmin(){
        return "select idLogAdministrador, actor, hora, fecha, datos, accion
                from LogAdministrador";
    }

    public function consultarTodosLogCli(){
        return "select idLogCliente, actor, hora, fecha, datos, accion
                from LogCliente";
    }
    
    public function consultarTodosLogProv(){
        return "select idLogProveedor, actor, hora, fecha, datos, accion
                from LogProveedor";
    }
    
    public function consultarFiltroAdmin($filtro){
        return "select idLogAdministrador, actor, hora, fecha, datos, accion
                from LogAdministrador
                where actor like '%" . $filtro . "%' or datos like '" . $filtro . "%' or accion like '" . $filtro . "%'";
    }
    
    public function consultarFiltroCli($filtro){
        return "select idLogCliente, actor, hora, fecha, datos, accion
                from LogCliente
                where actor like '%" . $filtro . "%' or datos like '" . $filtro . "%' or accion like '" . $filtro . "%'";
    }
    
    public function consultarFiltroProv($filtro){
        return "select idLogProveedor, actor, hora, fecha, datos, accion
                from LogProveedor
                where actor like '%" . $filtro . "%' or datos like '" . $filtro . "%' or accion like '" . $filtro . "%'";
    }
    
}







?>